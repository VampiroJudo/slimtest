<?php

declare(strict_types=1);

use Slim\Factory\AppFactory;
use Psr\Http\Message\ResponseInterface;
use App\Application\Middleware\AfterMiddleware;
use App\Application\Middleware\BeforeMiddleware;
use Psr\Http\Message\ServerRequestInterface as RequestInterface;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();

// Middleware for Error Reporting | Setup before routes
$app->addErrorMiddleware(true, true, false);

//Define app routes
$app->get('/hello/{name}', function (RequestInterface $request, ResponseInterface $response, $args) {

	$name = $args['name'];
	$response->getBody()->write("Hello, {$name}");

	return $response;

})->add(new AfterMiddleware())
	->add(new BeforeMiddleware());

// Define user route
$app->get('/user/{name}', function(RequestInterface $request, ResponseInterface $response, $args) {

	$name = $args['name'];
	$response->getBody()->write('Hello, $name');

	return $response;
});

// Run app
$app->run();